module.exports = function (gulp, config, log, $) {

  var wiredep = require('wiredep').stream;

  return {
    injectBowerIntoIndex: injectBowerIntoIndex,
    injectJSIntoIndex: injectJSIntoIndex
  };
  
  function injectBowerIntoIndex() {
    log('Wiring the bower dependencies into the html');
    return gulp
      .src(config.index)
      .pipe(wiredep())
      .pipe(gulp.dest(config.src));
  }

  function injectJSIntoIndex() {
    log('Wiring the js dependencies into the html');
    var sourcesOptions = {read: false};
    var sources = gulp.src(config.allOrderedJSFiles, sourcesOptions);
    return gulp
      .src(config.index)
      .pipe($.inject(sources, config.injectOptions))
      .pipe(gulp.dest(config.src));
  }

}
