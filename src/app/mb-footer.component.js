(function() {
  "use strict";

  angular
    .module('app')
    .component('mbFooter', {
      templateUrl: 'app/mb-footer.component.html',
      controllerAs: 'model',
      controller: Controller
    });

  function Controller() {
    var model = this;
    model.address = {
      companyName: 'P.H.U. Marpek Bis Biuro Promocji i Reklamy'
    }
  }
})();
