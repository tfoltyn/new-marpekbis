'use strict';
describe('appConfigServiceCache', function(){
  
  var service;

  beforeEach(function(){
    module('app.config');
    inject(function($injector){
      service = $injector.get('appConfigServiceCache');
    });
  });

  it('is empty when not defined', function(){
    expect(service.isEmpty()).toBeTruthy();
  });

  it('is not empty when defined', function(){
    service.setCache('test');
    expect(service.isEmpty()).toBeFalsy();
  });
});
