'use strict';
describe('appConfigService', function(){

  var service, $rootScope, $httpBackend;
  var providerMock = {
    configUrl: '/config/config.json'
  };
  var cacheMock = {
    isEmpty: null,
    setCache: null,
    getCache: null
  };

  beforeEach(function(){
    module('app.config', function($provide){
      $provide.value('appConfigServiceProvider', providerMock);
      $provide.value('appConfigServiceCache', cacheMock);
    });
    inject(function($injector){
      $rootScope = $injector.get('$rootScope');
      $httpBackend = $injector.get('$httpBackend');
      service = $injector.get('appConfigService');
    });
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should get data from cache if possible', function(){
    //given
    spyOn(cacheMock, 'isEmpty').and.callFake(function(){return false;});
    spyOn(cacheMock, 'getCache').and.callFake(function(){return 'test';});
    //when
    service.getConfig().then(function(config){
      //then
      expect(config).toEqual('test');
    });
    $rootScope.$apply();
  });

  it('should get data from http and cache it if cache is empty', function(){
    //given
    var testData = 'testData';
    $httpBackend.expect('GET', providerMock.configUrl).respond(testData);
    spyOn(cacheMock, 'isEmpty').and.callFake(function(){return true;});
    spyOn(cacheMock, 'setCache');
    //when
    service.getConfig().then(function(config){
      //then
      expect(config).toEqual(testData);
      expect(cacheMock.setCache).toHaveBeenCalledWith(testData);
    });
    $httpBackend.flush();

  });
});
