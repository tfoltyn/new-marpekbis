(function(){
  "use strict";

  angular
    .module('app.config')
    .service('appConfigService', Service);

  Service.$inject = ['$http', '$q', 'appConfigServiceProvider', 'appConfigServiceCache'];

  function Service($http, $q, appConfigServiceProvider, appConfigServiceCache) {
    var service = this;

    service.getConfig = getConfig;

    function getConfig() {
      return (appConfigServiceCache.isEmpty())? getFromFile() : getFromCache();
    }

    function getFromFile() {
      return $http.get(appConfigServiceProvider.configUrl).then(function(response){
        appConfigServiceCache.setCache(response.data);
        return response.data;
      });
    }

    function getFromCache() {
      var defer = $q.defer();
      defer.resolve(appConfigServiceCache.getCache());
      return defer.promise;
    }
  }
})();
