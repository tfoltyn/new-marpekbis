(function(){
  "use strict";

  angular
    .module('app.config')
    .service('appConfigServiceCache', Service);

  function Service() {
    var service = this;
    var cache = null;

    service.isEmpty = isEmpty;
    service.setCache = setCache;
    service.getCache = getCache;

    function isEmpty() {
      return cache === null;
    }

    function setCache(value) {
      cache = value;
    }

    function getCache() {
      return cache;
    }
  }
})();
