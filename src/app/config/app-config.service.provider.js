(function(){
  "use strict";

  angular
    .module('app.config')
    .factory('appConfigServiceProvider', function(){
      return {
        configUrl: 'config/config.json'
      }
    });
})();
