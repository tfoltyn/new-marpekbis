(function(){
    "use strict";

  angular
    .module('app')
    .component('mbApp', {
      templateUrl: 'app/mb-app.component.html',
      $routeConfig: [
        { path: '/home', component: 'homePage', name: 'Home'},
        { path: '/clients', component: 'clientsPage', name: 'Clients'},
        { path: '/company', component: 'companyPage', name: 'Company'},
        { path: '/contact', component: 'contactPage', name: 'Contact'},
        { path: '/realizations', component: 'realizationsPage', name: 'Realizations'},
        { path: '/rent', component: 'rentPage', name: 'Rent'},
        { path: '/**', redirectTo: ['Home']}
      ],
      controllerAs: 'model',
      controller: Controller
    });

  function Controller() {
    var model = this;
    model.setActiveRoute = setActiveRoute;
    
    function setActiveRoute(value) {
      model.activeRouteName = value;
    }
  }
})();
