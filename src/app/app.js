(function() {
  'use strict';
  
  var module = angular.module('app',
   ['ngComponentRouter', 'home', 'company', 'contact', 'realizations', 'rent', 'shared', 'clients',
   'app.config']
  );
  module.value('$routerRootComponent', 'mbApp');

})();
