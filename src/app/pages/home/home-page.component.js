(function(){
    "use strict";

  angular
    .module('home')
    .component('homePage', {
      templateUrl: 'app/pages/home/home-page.component.html',
      require: {
        "parent": "^mbApp"
      },
      controllerAs: 'model',
      controller: ['appConfigService', Controller]
    });

  function Controller(appConfigService) {
    var model = this;
    model.$routerOnActivate = routerOnActivate;
    model.$onInit = onInit;

    function onInit() {
      appConfigService.getConfig().then(handleConfigloadSuccess);
    }

    function handleConfigloadSuccess(config) {
      model.jumbotron = config.jumbotron;
      model.whatWeDo = config.whatWeDo;
      model.ourClients = config.ourClients;
      model.homeRent = config.homeRent;
      model.contact = config.contact;
    }

    function routerOnActivate(next) {
      model.parent.setActiveRoute(next.urlPath);
    }
  }
})();
