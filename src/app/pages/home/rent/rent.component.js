(function(){
  "use strict";

  angular
    .module('home')
    .component('homeRent', {
      templateUrl: 'app/pages/home/rent/rent.component.html',
      controllerAs: 'model',
      bindings: {
        header: '<',
        message: '<',
        btnInfo: '<',
        btnLabel: '<',
        btnLink: '@',
        image: '<'
      }
    });
})();
