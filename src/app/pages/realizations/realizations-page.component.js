(function(){
  "use strict";

  angular
    .module('realizations')
    .component('realizationsPage', {
      templateUrl: 'app/pages/realizations/realizations-page.component.html',
      require: {
        "parent": "^mbApp"
      },
      controllerAs: 'model',
      controller: ['appConfigService', Controller]
    });

  function Controller(appConfigService) {
    var model = this;
    model.$onInit = onInit;
    model.$routerOnActivate = routerOnActivate;

    function onInit() {
      appConfigService.getConfig().then(handleConfigloadSuccess);
    }

    function handleConfigloadSuccess(config) {
      model.size = config.realizations.size;
      model.items = config.realizations.items;
    }

    function routerOnActivate(next) {
      model.parent.setActiveRoute(next.urlPath);
    }
  }

})();

