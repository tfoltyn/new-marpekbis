(function(){
  "use strict";

  angular
    .module('company')
    .component('companyPage', {
      templateUrl: 'app/pages/company/company-page.component.html',
      require: {
        "parent": "^mbApp"
      },
      controllerAs: 'model',
      controller: ['appConfigService', Controller]
    });

  function Controller(appConfigService) {
    var model = this;
    model.$onInit = onInit;

    function onInit() {
      appConfigService.getConfig().then(handleConfigloadSuccess);
    }

    function handleConfigloadSuccess(config) {
      model.whatWeDo = config.whatWeDo;
    }

    model.$routerOnActivate = function(next) {
      model.parent.setActiveRoute(next.urlPath);
    }
  }
})();

