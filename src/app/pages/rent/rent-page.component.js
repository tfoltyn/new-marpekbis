(function(){
  "use strict";

  angular
    .module('rent')
    .component('rentPage', {
      templateUrl: 'app/pages/rent/rent-page.component.html',
      require: {
        "parent": "^mbApp"
      },
      controllerAs: 'model',
      controller: ['appConfigService', Controller]
    });

  function Controller(appConfigService) {
    var model = this;
    model.$onInit = onInit;
    model.$routerOnActivate = routerOnActivate;

    function onInit() {
      appConfigService.getConfig().then(handleConfigloadSuccess);
    }

    function handleConfigloadSuccess(config) {
      model.size = config.rent.size;
      model.items = config.rent.items;
    }

    function routerOnActivate (next) {
      model.parent.setActiveRoute(next.urlPath);
    }
  }

})();

