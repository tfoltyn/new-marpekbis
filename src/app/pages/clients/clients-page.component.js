(function(){
  "use strict";

  angular
    .module('clients')
    .component('clientsPage', {
      templateUrl: 'app/pages/clients/clients-page.component.html',
      require: {
        "parent": "^mbApp"
      },
      controllerAs: 'model',
      controller: ['appConfigService', Controller]
    });
  
  function Controller(appConfigService) {
    var model = this;
    model.$onInit = onInit;

    function onInit() {
      appConfigService.getConfig().then(handleConfigloadSuccess);
    }

    function handleConfigloadSuccess(config) {
      model.ourClients = config.ourClients;
    }

    model.$routerOnActivate = function(next) {
      model.parent.setActiveRoute(next.urlPath);
    }
  }
})();

