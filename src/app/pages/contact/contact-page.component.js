(function(){
  "use strict";

  angular
    .module('contact')
    .component('contactPage', {
      templateUrl: 'app/pages/contact/contact-page.component.html',
      require: {
        "parent": "^mbApp"
      },
      controllerAs: 'model',
      controller: ['appConfigService', Controller]
    });

  function Controller(appConfigService) {
    var model = this;
    model.$onInit = onInit;

    function onInit() {
      appConfigService.getConfig().then(handleConfigloadSuccess);
    }

    function handleConfigloadSuccess(config) {
      model.contact = config.contact;
    }
    
    model.$routerOnActivate = function(next) {
      model.parent.setActiveRoute(next.urlPath);
    }
  }
  
})();

