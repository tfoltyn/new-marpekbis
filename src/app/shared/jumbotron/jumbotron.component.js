(function(){
  "use strict";
  
  angular
    .module('shared')
    .component('jumbotron', {
      templateUrl: 'app/shared/jumbotron/jumbotron.component.html',
      controllerAs: 'model',
      bindings: {
        header: '<',
        message: '<',
        btnLink: '@',
        btnLabel: '<',
        image: '<'
      },
      controller: Controller
    });
  
  function Controller() {
    var model = this;
  }
})();
