describe('Gallery pagination service', function(){
  'use strict';

  var service;
  var items = new Array(25);
  var configuration = [
    {page: 0, size: 10, result: 0},
    {page: 1, size: 10, result: 10},
    {page: 2, size: 10, result: 10},
    {page: 3, size: 10, result: 5},
    {page: 4, size: 10, result: 0}
  ];

  beforeEach(function(){
    module('shared');
    inject(function($injector){
      service = $injector.get('galleryPaginationService');
    });
  });

  it('should return targeted amount of items depending on configuration', function(){
    configuration.forEach(function(config){
      //given
      //when
      var result = service.getItemsForPage(items, config.page, config.size);
      //then
      expect(result.length).toEqual(config.result);
    });
  });
});
