describe('Gallery filter service', function(){
  'use strict';
  var service;

  beforeEach(function(){
    module('shared');
    inject(function($injector){
      service = $injector.get('galleryFilterService');
    });
  });

  it('should return all items if item city param equals Wszystkie', function(){
    //given
    var items = [
      {city: 'Gdańsk'},
      {city: 'Gdynia'},
      {city: 'Sopot'}
    ];
    //when
    var result = service.filterItemsByCity(items, 'Wszystkie');
    //then
    expect(result.length).toEqual(3);
  });

  it('should return only items when item param city match with provided value', function(){
    //given
    var items = [
      {city: 'Gdańsk'},
      {city: 'Gdynia'},
      {city: 'Sopot'}
    ];
    //when
    var result = service.filterItemsByCity(items, 'Sopot');
    //then
    expect(result.length).toEqual(1);
  });

  it('should return nothing when all items param city do not match with provided value', function(){
    //given
    var items = [
      {city: 'Gdańsk'},
      {city: 'Gdynia'},
      {city: 'Sopot'}
    ];
    //when
    var result = service.filterItemsByCity(items, 'Wejherowo');
    //then
    expect(result.length).toEqual(0);
  });

  it('should return nothing when all items are empty array', function(){
    //given
    var items = [];
    //when
    var result = service.filterItemsByCity(items, 'Kartuzy');
    //then
    expect(result.length).toEqual(0);
  });

  it('should return nothing when items does not have city param', function(){
    //given
    var items = [
      {state: 'Gdańsk'},
      {state: 'Gdynia'},
      {state: 'Sopot'}
    ];
    //when
    var result = service.filterItemsByCity(items, 'Gdynia');
    //then
    expect(result.length).toEqual(0);
  });
});

