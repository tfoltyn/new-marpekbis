(function() {
  "use strict";

  angular
    .module('shared')
    .service('galleryFilterService', Service);

  function Service() {
    var service = this;
    var ALL_CITIES = 'Wszystkie';
    service.filterItemsByCity = filterItemsByCity;

    function filterItemsByCity(items, city) {
      var result = [];
      if(city === ALL_CITIES){
        result = angular.copy(items);
      }else {
        result = items.filter(function(item){
          return item.city === city;
        });
      }
      return result;
    }
  }
})();

