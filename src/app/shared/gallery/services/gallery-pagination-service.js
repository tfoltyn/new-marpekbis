(function() {
  "use strict";
  
  angular
    .module('shared')
    .service('galleryPaginationService', Service);
  
  function Service() {
    var service = this;
    service.getItemsForPage = getItemsForPage;

    function getItemsForPage(items, page, size) {
      var startIndex = (page === 1)? 0: ((page -1) * size);
      return items.slice(startIndex, startIndex + size);
    }
  }
})();
