(function() {
  "use strict";

  angular
    .module("shared")
    .component("galleryModal", {
      templateUrl: 'app/shared/gallery/components/gallery-modal.component.html',
      bindings: {
        item: '<'
      },
      controllerAs: 'model'
    });
})();
