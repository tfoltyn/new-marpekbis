(function(){
    "use strict";

  angular
    .module('shared')
    .component('galleryPagination', {
      templateUrl: 'app/shared/gallery/components/gallery-pagination.component.html',
      bindings: {
        pages: '<',
        activePage: '<',
        onChange: '&'
      },
      controllerAs: 'model',
      controller: Controller
    });

  function Controller() {
    var model = this;
    model.changePage = changePage;
    model.isPrevButtonDisabled = isPrevButtonDisabled;
    model.isNextButtonDisabled = isNextButtonDisabled;
    model.goToPrevPage = goToPrevPage;
    model.goToNextPage = goToNextPage;
    model.isPageActive = isPageActive;
    model.isVisible = isVisible;

    function changePage(value) {
      model.onChange({page: value});
    }

    function isPrevButtonDisabled() {
      return model.activePage < 2;
    }

    function isNextButtonDisabled() {
      return model.activePage === model.pages.length;
    }

    function goToPrevPage() {
      model.onChange({page: model.activePage - 1});
    }

    function goToNextPage() {
      model.onChange({page: model.activePage + 1});
    }

    function isPageActive(page) {
      return page === model.activePage;
    }

    function isVisible() {
      return model.pages.length > 1;
    }
  }

})();
