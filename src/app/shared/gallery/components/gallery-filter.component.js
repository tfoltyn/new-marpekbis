(function(){
  "use strict";

  angular
    .module('shared')
    .component('galleryFilter', {
      templateUrl: 'app/shared/gallery/components/gallery-filter.component.html',
      bindings: {
        cities: '<',
        activeCity: '<',
        onChange: '&'
      },
      controllerAs: 'model',
      controller: Controller
    });

  function Controller() {
    var model = this;
    model.onButtonClick = onButtonClick;
    model.isCityActive = isCityActive;

    function isCityActive(city) {
      return city === model.activeCity;
    }
    
    function onButtonClick(value){
      model.onChange({city: value});
    }
  }

})();
