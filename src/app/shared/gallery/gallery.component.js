(function(){
  "use strict";
  
  angular
    .module('shared')
    .component('gallery', {
      templateUrl: 'app/shared/gallery/gallery.component.html',
      bindings: {
        items: '<',
        size: '<',
        type: '@'
      },
      controllerAs: 'model',
      controller: [
       'galleryPagesFactory', 'galleryFilterService', 'galleryFiltersFactory', 'galleryPaginationService',
        Controller
      ]
    });
  
  function Controller(galleryPagesFactory, galleryFilterService, galleryFiltersFactory,
                      galleryPaginationService) {
    var model = this;
    var modal;
    model.$onInit = onInit;
    model.$onChanges = onChanges;
    model.changePage = changePage;
    model.changeCityFilter = changeCityFilter;
    model.onListItemClick = onListItemClick;
    model.isGrid = isGrid;
    model.isList = isList;

    function onInit() {
      modal = $("#bigImageModal");
    }

    function onChanges() {
      if(model.size && model.items){
        initFilters();
        filterItems();
        initPaginationData();
        trimItemsToPage(model.activePage);
      }
    }

    function initFilters() {
      model.cities = galleryFiltersFactory.createCities(model.items);
      model.activeCity = model.cities[0];
    }

    function changePage(value) {
      model.activePage = value;
      trimItemsToPage(model.activePage);
    }
    
    function changeCityFilter(newCity) {
      model.activeCity = newCity;
      filterItems();
      initPaginationData();
      trimItemsToPage(model.activePage);
    }

    function filterItems() {
      model.filteredItems = galleryFilterService.filterItemsByCity(model.items, model.activeCity);
    }
    
    function initPaginationData() {
      model.activePage = 1;
      model.pages = galleryPagesFactory.createPages(model.filteredItems, model.size);
    }

    function trimItemsToPage(page) {
      model.paginatedItems = galleryPaginationService.getItemsForPage(model.filteredItems, page, model.size);
    }

    function onListItemClick(item){
      model.activeItem = item;
      modal.modal('show');
    }

    function isGrid() {
      return model.type === 'grid';
    }

    function isList() {
      return model.type === 'list';
    }
  }
})();
