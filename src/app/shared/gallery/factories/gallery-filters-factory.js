(function() {
  "use strict";

  angular
    .module('shared')
    .service('galleryFiltersFactory', Service);

  function Service() {
    var service = this;
    var ALL_CITIES = 'Wszystkie';
    service.createCities = createCities;

    function createCities(items) {
      var filters = [ALL_CITIES];
      items.map(function(item){
        if(filters.indexOf(item.city) === -1){
          filters.push(item.city);
        }
      });
      return filters;
    }
  }
})();
