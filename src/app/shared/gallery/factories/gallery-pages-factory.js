(function() {
  "use strict";

  angular
    .module('shared')
    .service('galleryPagesFactory', Service);

  function Service() {
    var service = this;
    service.createPages = createPages;

    function createPages(items, size) {
      var pagesCount = Math.ceil(items.length/size);
      var pages = [];
      for(var i=1; i<=pagesCount; i++){
        pages.push(i);
      }
      return pages;
    }
  }
})();
