(function(){
  "use strict";

  angular
    .module('shared')
    .component('galleryListItem', {
      templateUrl: 'app/shared/gallery/list-items/gallery-list-item.component.html',
      bindings: {
        item: '<',
        onClick: '&'
      },
      controllerAs: 'model',
      controller: Controller
    });

  function Controller() {
    var model = this;
    model.onItemClick = onItemClick;
    model.goToGoogleMaps = goToGoogleMaps;

    function onItemClick() {
      model.onClick({item: model.item});
    }

    function goToGoogleMaps() {
      window.open(model.item.googleMapsUrl);
    }
  }

})();
