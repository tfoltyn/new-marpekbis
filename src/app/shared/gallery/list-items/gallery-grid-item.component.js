(function(){
  "use strict";

  angular
    .module('shared')
    .component('galleryGridItem', {
      templateUrl: 'app/shared/gallery/list-items/gallery-grid-item.component.html',
      bindings: {
        item: '<',
        onClick: '&'
      },
      controllerAs: 'model',
      controller: Controller
    });

  function Controller() {
    var model = this;
    model.onItemClick = onItemClick;

    function onItemClick() {
      model.onClick({item: model.item});
    }
  }
  
})();
