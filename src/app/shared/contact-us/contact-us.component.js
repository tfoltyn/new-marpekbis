(function(){
  "use strict";

  angular
    .module('shared')
    .component('contactUs', {
      templateUrl: 'app/shared/contact-us/contact-us.component.html',
      controllerAs: 'model',
      bindings: {
        header: '<',
        address: '<'
      },
      controller: Controller
    });

  function Controller() {
    var model = this;
    model.sendMessage = sendMessage;

    function sendMessage() {

    }
  }
})();
