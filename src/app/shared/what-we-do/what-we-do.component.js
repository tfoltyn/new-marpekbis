(function(){
  "use strict";

  angular
    .module('shared')
    .component('whatWeDo', {
      templateUrl: 'app/shared/what-we-do/what-we-do.component.html',
      controllerAs: 'model',
      bindings: {
        header: '<',
        message: '<',
        items: '<'
      }
    });
})();