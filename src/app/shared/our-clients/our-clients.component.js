(function(){
  "use strict";

  angular
    .module('shared')
    .component('ourClients', {
      templateUrl: 'app/shared/our-clients/our-clients.component.html',
      controllerAs: 'model',
      bindings: {
        header: '<',
        message: '<',
        items: '<'
      }
    });
})();
