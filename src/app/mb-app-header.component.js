(function(){
    "use strict";

  angular
    .module('app')
    .component('mbAppHeader', {
      templateUrl: 'app/mb-app-header.component.html',
      controllerAs: 'model',
      controller: Controller,
      bindings: {
        currentPage: '<'
      }
    });

  function Controller() {
    var model = this;
    model.isPageActive = isPageActive;

    function isPageActive(name) {
      return name === model.currentPage;
    }
  }

})();
